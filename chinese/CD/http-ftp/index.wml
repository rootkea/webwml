#use wml::debian::cdimage title="通过 HTTP/FTP 下载 Debian CD/DVD 映像" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

<div class="tip">
<p><strong>请不要像下载其他[CN:文件:][HKTW:档案:]那样\
用您的浏览器下载 CD 或 DVD 映像！</strong>原因是，如果下载中断，\
大多数浏览器不允许您从失败的地方重新开始下载。</p>
</div>

<p>作为替代，请使用支持断点续传的工具，它们通常被称为 <q>下载管理程序</q>。\
有很多浏览器插件能做到这一点，您也可能想要为此安装一个单独的程序。在 Linux/Unix \
系统中，可以使用 <a href="http://aria2.sourceforge.net/">aria2</a>、<a href="http://dfast.sourceforge.net/">wxDownload Fast</a> 或者（在命令行中）\
<q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q>或\
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>。\
还有更多的选项在<a
 href="https://zh.wikipedia.org/wiki/%E4%B8%8B%E8%BD%BD%E7%AE%A1%E7%90%86%E7%A8%8B%E5%BA%8F%E6%AF%94%E8%BE%83">下载管理程序比较</a>中被列出。</p>

<p>以下 Debian 映像可供\
下载：</p>

<ul>

  <li><a href="#stable"><q>稳定（stable）</q>版本的官方 CD/DVD 映像</a></li>
  
  <li><a href="#firmware">含有<b>非自由（non-free）</b>固件的<b>非官方</b><q>稳定版 </q>CD/DVD 镜像</a></li>
  
  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/"><q>测试（testing）</q>版本的官方 CD/DVD 映像（<em>每周重新生成</em>）</a></li>

</ul>

<p>也请参阅：</p>
<ul>

  <li>完整的 <a href="#mirrors"><tt>debian-cd/</tt> 镜像列表</a></li>

  <li>获取<q>网络安装</q>（150-300&nbsp;MB）映像，\
请参见<a href="../netinst/">网络安装</a>页面。</li>

  <li>获取<q>测试（testing）</q>版本的<q>网络安装</q>映像，\
包含每日构建以及已确认可工作的快照，参见 <a
  href="$(DEVEL)/debian-installer/">Debian 安装[CN:程序:][HKTW:程式:]页面</a>。</li>

</ul>

<hr />

<h2><a name="stable"><q>稳定（stable）</q>版本的官方 CD/DVD 映像</a></h2>

<p>要在没有互联网连接的计算机上安装 Debian，\
可以使用 CD 映像（每张 650&nbsp;MB）或 DVD 映像（每张 4.4&nbsp;GB）。\
下载第一个 CD 或 DVD 映像文件，用 CD/DVD 刻录机（对于 i386 和 amd64 移植，也可以用 [CN:U 盘:][HK:USB 手指:][TW:USB 随身碟:]）\
写入，然后用它重新引导系统。</p>

<p><strong>第一张</strong> CD/DVD 包含了安装标准 Debian 系统\
需要的全部[CN:文件:][HKTW:档案:]。<br />
为了避免不必要的下载，请<strong>不要</strong>下载\
其他 CD 或 DVD 映像，除非您需要它们包含的\
软件包。</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>以下链接指向的映像大小不超过 650&nbsp;MB，\
适合写入正常的 CD-R(W) [CN:介质:][HKTW:媒介:]：</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>以下链接指向的映像大小不超过 4.4&nbsp;GB，\
适合写入正常的 DVD-R/DVD+R 及类似[CN:介质:][HKTW:媒介:]：</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>请您务必在安装前阅读文档。\
<strong>如果您在安装前只想阅读一份文档</strong>，请阅读我们的\
<a href="$(HOME)/releases/stable/i386/apa">安装指南</a>，这是一份\
安装过程的简要介绍。其他有用的文档包括：
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">安装手册</a>，
详细的安装步骤</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian 安装[CN:程序:][HKTW:程式:]\
文档</a>，包括常见问题及解答（FAQ）</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian 安装\
[CN:程序:][HKTW:程式:]勘误</a>，安装[CN:程序:][HKTW:程式:]的已知问题列表</li>
</ul>

<hr />

# 译者注意: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">包含非自由固件的非官方 CD/DVD 镜像</a></h2>

<div id="firmware_nonfree" class="important">
<p>
如果你电脑中的任何硬件需要需要驱动加载<strong>非自由固件</strong>，你可以 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
下载公共固件 tarballs </a>或下载一个包含了<strong>非自由</strong>固件的<strong>非官方的</strong>镜像。如何使用 tarballs 与如何在安装过程中加载
固件的说明可以参阅
<a href="../../releases/stable/amd64/ch06s04">安装指南</a>。
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">包含了<strong>非自由</strong>固件的<strong>非官方的</strong>镜像</a>
</p>
</div>

<hr />

<h2><a name="mirrors"><q>debian-cd</q> 档案库的已注册镜像</a></h2>

<p>请注意<strong>有些镜像站不是最新的</strong> &mdash;
下载之前，请确认映像[CN:文件:][HKTW:档:]的版本\
与<a href="../#latest">这里</a>列出的相同！
此外，请注意许多镜像站由于体积原因并不包含所有的映像\
（尤其是 DVD 映像）。</p>

<p><strong>如有疑问，请使用位于瑞典的\
<a href="https://cdimage.debian.org/debian-cd/">主要 \
CD 映像服务器</a>，</strong>或者尝试\
<a href="http://debian-cd.debian.net/">实验性的自动\
镜像选择器</a>，它可以自动将您重定向到\
附近的一个已知含有最新版本的镜像站。</p>

<p>您是否对在您的镜像站提供 Debian CD 映像\
感兴趣？如果是的话，请参阅<a href="../mirroring/">如何
搭建 CD 映像[CN:文件:][HKTW:档:]镜像的步骤说明</a>。</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"


