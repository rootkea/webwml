#use wml::debian::translation-check translation="c9738f32e0f680229ccd60cbb80ff7926f0e59a0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Dans la mise à jour de sécurité de systemd récemment publiée (215-17+deb8u12
à l’aide de DLA-1762-1), une régression a été découverte dans le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2017-18078">CVE-2017-18078</a>.</p>

<p>L’observation d’utilisateurs de Jessie LTS de Debian est qu’après la mise
à niveau vers +deb8u12, les fichiers temporaires n’avaient plus les propriétaires
et les permissions correctes (au lieu de fichiers possédés par un utilisateur ou
un groupe particulier, les fichiers étaient possédés par root:root ; le réglage
des permissions POSIX de fichier [rwx, etc.] était aussi affecté).</p>


<p>Pour Debian 8 <q>Jessie</q>, ce problème de régression a été corrigé dans la
version 215-17+deb8u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p> Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été résolus dans la
version 215-17+deb8u1 de systemd.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1762-2.data"
# $Id: $
