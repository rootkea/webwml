#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le serveur HTTPD d’Apache. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3167">CVE-2017-3167</a>

<p>Emmanuel Dreyfus a signalé que l’utilisation de ap_get_basic_auth_pw() par
des modules tiers en dehors de la phase d’authentification pourrait conduire au
contournement des exigences d’authentification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3169">CVE-2017-3169</a>

<p>Vasileios Panopoulos de AdNovum Informatik_AG a découvert que mod_ssl peut
déréférencer un pointeur NULL lorsque des modules tiers appellent
ap_hook_process_connection() lors d’une requête HTTP vers un port HTTPS
conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7668">CVE-2017-7668</a>

<p>Javier Jimenez a signalé que l’analyse stricte d’HTTP contient un défaut
conduisant à une lecture excessive de tampon dans ap_find_token(). Un attaquant
distant peut exploiter ce défaut en contrefaisant soigneusement une séquence
d’en-têtes de requête pour provoquer une erreur de segmentation, ou pour
obliger ap_find_token() à renvoyer une valeur incorrecte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7679">CVE-2017-7679</a>

<p>ChenQin et Hanno Boeck ont signalé que mod_mime peut lire un octet de plus à la
fin du tampon lors de l’envoi d’un en-tête de réponse Content-Type malveillant.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.2.22-13+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1009.data"
# $Id: $
