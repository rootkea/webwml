#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans rtmpdump et la bibliothèque librtmp.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8270">CVE-2015-8270</a>

<p>Un bogue dans AMF3ReadString dans librtmp peut provoquer un déni de service
à l’aide d’un plantage d'application pour les utilisateurs de librtmp s’adressant
à un serveur malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8271">CVE-2015-8271</a>

<p>La fonction AMF3_Decode dans librtmp ne valide pas correctement son entrée.
Cela peut conduire à l’exécution de code arbitraire lors de dialogue avec un
attaquant malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8272">CVE-2015-8272</a>

<p>Un bogue dans rtmpsrv peut conduire à un plantage lors de dialogue avec un
client malveillant.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.4+20111222.git4e06e21-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rtmpdump.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-917.data"
# $Id: $
