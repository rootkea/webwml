#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet simplesamlphp dans wheezy est vulnérable à plusieurs attaques du
code relatif à l’authentification, conduisant à un accès non autorisé et à une
divulgation d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12867">CVE-2017-12867</a>

<p>La classe SimpleSAML_Auth_TimeLimitedToken permet à des attaquants ayant
accès à un jeton secret d’étendre sa période de validité en manipulant le
décalage temporel en préfixe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12869">CVE-2017-12869</a>

<p>Le module multiauth permet à des attaquants distants de contourner des
restrictions de contexte d'authentification et utiliser une source
d’authentification définie dans config/authsources.php à l’aide de vecteurs
relatifs à une validation incorrecte de saisie d’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a>

<p>(1) La source d’authentification Htpasswd dans le module authcrypt et (2)
la classe SimpleSAML_Session dans SimpleSAMLphp 1.14.11 et précédents permettent
à des attaquants distants de mener des attaques temporelles par canal auxiliaire
en exploitant l’utilisation de l’opérateur de comparaison standard pour comparer
le document secret avec la saisie de l’utilisateur.</p>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2017-12868">CVE-2017-12868</a>
concernait un correctif inapproprié de
<a href="https://security-tracker.debian.org/tracker/CVE-2017-12872">CVE-2017-12872</a>
dans la correction initiale publiée par l’amont. Nous avons utilisé le correctif
approprié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12873">CVE-2017-12873</a>

<p>SimpleSAMLphp peut permettre à des attaquants d’obtenir des informations
sensibles, un accès non autorisé ou de provoquer un impact non précisé en
exploitant une génération de NameID persistante et incorrecte lorsqu’un IdP
(Identity Provider) est mal configuré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12874">CVE-2017-12874</a>

<p>Le module InfoCard pour SimpleSAMLphp permet à des attaquants d’usurper les
messages XML en exploitant une vérification incorrecte de valeurs de retour dans
les utilitaires de validation de signature.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.9.2-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets simplesamlphp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1205.data"
# $Id: $
