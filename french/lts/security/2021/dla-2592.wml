#use wml::debian::translation-check translation="6669c6a454ffcf1656bd6cf0ebd11a26a26fca95" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le langage de programmation
Go. Un attaquant pourrait déclencher un déni de service (DoS), un contournement
du contrôle d’accès et exécuter du code arbitraire sur l’ordinateur du
développeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15041">CVE-2017-15041</a>

<p>Go permet une exécution de commande à distance <q>go get</q>. En utilisant
des domaines personnalisés, il est possible d’obtenir que example.com/pkg1
pointe vers un dépôt Subversion mais que example.com/pkg1/pkg2 pointe
vers un dépôt Git. Si le dépôt Subversion inclut une extraction Git dans son
répertoire pkg2 et qu’une autre action est faite pour assurer un arrangement
correct des opérations, <q>go get</q> peut être piégé pour réutiliser cette
extraction Git pour rechercher du code de pkg2. Si l’extraction Git du dépôt
Subversion possède des commandes malveillantes dans .git/hooks/, elles seront
exécutées sur le système exécutant <q>go get</q></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16873">CVE-2018-16873</a>

<p>La commande <q>go get</q> est vulnérable à une exécution de code à distance
lorsqu’elle est exécutée avec l’argument -u flag et si le chemin d’importation
d’un paquet Go est malveillant, comme il peut traiter le répertoire parent comme une
racine de dépôt Git, contenant une configuration malveillante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16874">CVE-2018-16874</a>

<p>La commande <q>go get</q> est vulnérable à une traversée de répertoires
lorsqu’elle est exécutée avec le chemin d’importation d’un paquet Go malveillant
qui contient des accolades (droite ou gauche). L’attaquant peut provoquer une
écriture arbitraire sur le système de fichiers, qui peut conduire à une
exécution de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9741">CVE-2019-9741</a>

<p>Dans net/http, une injection CRLF est possible si l’attaquant contrôle un
paramètre d’URL, comme le montre le second argument de http.NewRequest avec
\r\n suivi par un en-tête HTTP ou une commande Redis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16276">CVE-2019-16276</a>

<p>Go permet la dissimulation de requête HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17596">CVE-2019-17596</a>

<p>Go peut paniquer lors d’un essai de traiter un trafic réseau contenant une
clé publique DSA non valable. Plusieurs scénarios d’attaque sont possibles tels
que le trafic d’un client vers un serveur qui vérifie les certificats de client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3114">CVE-2021-3114</a>

<p>crypto/elliptic/p224.go peut générer des sorties incorrectes, relatives à un
dépassement de capacité par le bas de la partie inférieure lors de la réduction
globale finale dans le champ P-224.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.8.1-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-1.8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-1.8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-1.8">\
https://security-tracker.debian.org/tracker/golang-1.8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2592.data"
# $Id: $
