<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update doesn't fix a vulnerability in linux-tools, but provides
support for building Linux kernel modules with the <q>retpoline</q>
mitigation for <a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a> (Spectre variant 2).</p>

<p>This update also includes bug fixes from the upstream Linux 3.2 stable
branch up to and including 3.2.101.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.101-1.</p>

<p>We recommend that you upgrade your linux-tools packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1349.data"
# $Id: $
