<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were found in Policykit, a framework for managing
administrative policies and privileges:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19788">CVE-2018-19788</a>

    <p>It was discovered that incorrect processing of very high UIDs in
    Policykit could result in authentication bypass.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6133">CVE-2019-6133</a>

    <p>Jann Horn of Google found that Policykit doesn't properly check
    if a process is already authenticated, which can lead to an
    authentication reuse by a different user.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.105-15~deb8u4.</p>

<p>We recommend that you upgrade your policykit-1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1644.data"
# $Id: $
