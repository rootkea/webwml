<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>OpenSSH secure shell client and server had a denial of service
vulnerability reported.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6515">CVE-2016-6515</a>

  <p>The password authentication function in sshd in OpenSSH before 7.3
  does not limit password lengths for password authentication, which
  allows remote attackers to cause a denial of service
  (crypt CPU consumption) via a long string.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this problems has been fixed in version
6.0p1-4+deb7u6.</p>

<p>We recommend that you upgrade your openssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-594.data"
# $Id: $
