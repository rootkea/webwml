<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0772">CVE-2016-0772</a>

      <p>A vulnerability in smtplib allowing MITM attacker to perform a
      startTLS stripping attack. smtplib does not seem to raise an
      exception when the remote end (smtp server) is capable of
      negotiating starttls but fails to respond with 220 (ok) to an
      explicit call of SMTP.starttls(). This may allow a malicious
      MITM to perform a startTLS stripping attack if the client code
      does not explicitly check the response code for startTLS.</p></li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5636">CVE-2016-5636</a>

      <p>Issue #26171: Fix possible integer overflow and heap corruption
      in zipimporter.get_data().</p></li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5699">CVE-2016-5699</a>

      <p>Protocol injection can occur not only if an application sets a
      header based on user-supplied values, but also if the application
      ever tries to fetch a URL specified by an attacker (SSRF case) OR
      if the application ever accesses any malicious web server
      (redirection case).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.7.3-6+deb7u3.</p>

<p>We recommend that you upgrade your python2.7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-522.data"
# $Id: $
