<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the GNOME Display Manager where
not detecting any users may make GDM launch initial system setup and thereby
permitting the creation of new users with sudo capabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16125">CVE-2020-16125</a>

    <p>display: Exit with failure if loading existing users fails</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
3.22.3-3+deb9u3.</p>

<p>We recommend that you upgrade your gdm3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2434.data"
# $Id: $
