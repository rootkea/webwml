<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>vorbis-tools is vulnerable to multiple issues that can result in denial
of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9638">CVE-2014-9638</a>

    <p>Divide by zero error in oggenc with a WAV file whose number of
    channels is set to zero.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9639">CVE-2014-9639</a>

    <p>Integer overflow in oggenc via a crafted number of channels in a WAV
    file, which triggers an out-of-bounds memory access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9640">CVE-2014-9640</a>

    <p>Out-of bounds read in oggenc via a crafted raw file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6749">CVE-2015-6749</a>

    <p>Buffer overflow in the aiff_open function in oggenc/audio.c
    via a crafted AIFF file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.0-1+deb7u1.</p>

<p>We recommend that you upgrade your vorbis-tools packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1010.data"
# $Id: $
