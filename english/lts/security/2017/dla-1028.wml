<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Robert Święcki discovered that the value placeholder in [Proxy-]Authorization
Digest headers were not initialized or reset before or between successive
key=value assignments in Apache 2's mod_auth_digest module</p>

<p>Providing an initial key with no '=' assignment could reflect the stale value
of uninitialized pool memory used by the prior request leading to leakage of
potentially confidential information and a segfault.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in apache2 version
2.2.22-13+deb7u10.</p>

<p>We recommend that you upgrade your apache2 packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1028.data"
# $Id: $
