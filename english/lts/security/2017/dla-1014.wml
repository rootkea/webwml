<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was an arbitrary code execution vulnerability in
libcamunrar, a library to add unrar support to the Clam anti-virus software.</p>

<p>This was caused by an integer overflow resulting in a negative value of the
``DestPos`` variable, which allows the attacker to write out of bounds when
setting ``Mem[DestPos]``.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in libclamunrar version
0.99-0+deb7u2.</p>

<p>We recommend that you upgrade your libclamunrar packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1014.data"
# $Id: $
