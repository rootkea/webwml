#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml newsevents\n"
"PO-Revision-Date: 2016-12-29 15:31+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr "Tin tức Debian"

#: ../../english/News/news.rdf.in:19
msgid "Debian Latest News"
msgstr "Tin tức Debian mới nhất"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "p<get-var page />"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr "Thư thông báo cho cộng đồng Debian"

#: ../../english/events/talks.defs:9
msgid "Title:"
msgstr "Tiêu đề:"

#: ../../english/events/talks.defs:12
msgid "Author:"
msgstr "Tác giả:"

#: ../../english/events/talks.defs:15
msgid "Language:"
msgstr "Ngôn ngữ:"

#: ../../english/events/talks.defs:19
msgid "Date:"
msgstr "Ngày:"

#: ../../english/events/talks.defs:23
msgid "Event:"
msgstr "Sự kiện:"

#: ../../english/events/talks.defs:26
msgid "Slides:"
msgstr "Trình chiếu:"

#: ../../english/events/talks.defs:29
msgid "source"
msgstr "nguồn"

#: ../../english/events/talks.defs:32
msgid "PDF"
msgstr "PDF"

#: ../../english/events/talks.defs:35
msgid "HTML"
msgstr "HTML"

#: ../../english/events/talks.defs:38
msgid "MagicPoint"
msgstr "MagicPoint"

#: ../../english/events/talks.defs:41
msgid "Abstract"
msgstr "Tổng quát"

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Các sự kiện hấp dẫn sắp diễn ra"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "liên kết không còn hợp lệ nữa"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "Khi"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Tại"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Thông tin thêm"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Liên quan đến Debian"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Người điều phối chính"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Dự án</th><th>Người điều phối</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Liên kết có liên quan"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Tin mới nhất"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr "Tải về mục lịch"

#: ../../english/template/debian/news.wml:9
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr ""
"Trở lại: <a href=\"./\">Tin tức Debian</a> khác || <a href=\"m4_HOME/"
"\">Trang chủ dự án Debian</a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (liên kết đã chết)"

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""
"Chào mừng bạn đến với ấn bản DPN <get-var issue /> của năm, thư thông báo "
"cho cộng đồng Debian. Các chủ đề trong phát hành này bao gồm:"

#: ../../english/template/debian/projectnews/boilerplates.wml:43
#, fuzzy
#| msgid ""
#| "Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
#| "the Debian community. Topics covered in this issue include:"
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community."
msgstr ""
"Chào mừng bạn đến với ấn bản DPN <get-var issue /> của năm, thư thông báo "
"cho cộng đồng Debian. Các chủ đề trong phát hành này bao gồm:"

#: ../../english/template/debian/projectnews/boilerplates.wml:49
msgid "Other topics covered in this issue include:"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:69
#: ../../english/template/debian/projectnews/boilerplates.wml:90
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""
"Theo như <a href=\"https://udd.debian.org/bugs.cgi\">giao diện Tìm kiếm Lỗi "
"của Ultimate Debian Database</a>, bản phát hành tiếp theo, Debian <q><get-"
"var release /></q>, thì hiện tại chịu ảnh hưởng bởi các lỗi Release-Critical "
"<get-var testing />. Bỏ qua các lỗi mà dễ dàng xử lý hay cách được xử lý, "
"đại khái thế, về các lỗi Release-Critical <get-var tobefixed /> còn lại được "
"sửa cho the release to happen."

#: ../../english/template/debian/projectnews/boilerplates.wml:70
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""
"Đây cũng còn có một số <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">gợi ý cách phiên dịch</a> những con số đó."

#: ../../english/template/debian/projectnews/boilerplates.wml:91
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""
"Ở đây cũng có <a href=\"<get-var url />\">nhiều thống kê chi tiết</a> cũng "
"như là một số <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">gợi ý "
"cách phiên dịch</a> những con số đó."

#: ../../english/template/debian/projectnews/boilerplates.wml:115
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""
"<a href=\"<get-var link />\">Hiện giờ</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\">có <get-var orphaned /> gói chưa có ai đỡ đầu</a> và <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> gói sắp mồ côi</a>: vui lòng xem toàn bộ danh "
"sách của <a href=\"m4_DEVEL/wnpp/help_requested\">các gói cần bạn trợ giúp</"
"a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:127
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""
"Vui lòng giúp chúng tôi tạo bản tin này. Chúng tôi vẫn cần nhiều hơn nữa "
"những người viết để theo dõi cộng đồng Debian và báo cáo về những gì đang "
"diễn ra. Vui lòng xem <a href=\"https://wiki.debian.org/ProjectNews/"
"HowToContribute\">trang đóng góp</a> để biết cách giúp đỡ. Chúng tôi mong "
"chờ nhận được thư của bạn tại địa chỉ thư điện tử <a href=\"mailto:debian-"
"publicity@lists.debian.org\">debian-publicity@lists.debian.org</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:188
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""
"Vui lòng chú ý rằng có phần của những cố vấn bảo mật quan trọng của những "
"tuần cuối cùng. Nếu bạn cần giữ những cố vấn bảo mật phát hành bở Nhóm Bảo "
"mật Debian luôn cập nhật, vui lòng đặt xem dài hạn <a href=\"<get-var url-"
"dsa />\">bó thư bảo mật</a> (và các bó rời <a href=\"<get-var url-bpo />"
"\">bó thư chuyển ngược</a>, và <a href=\"<get-var url-stable-announce />"
"\">bó thư cập nhật ổn định</a>) để biết các thông báo."

#: ../../english/template/debian/projectnews/boilerplates.wml:189
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""
"Vui lòng chú ý rằng có phần của những cố vấn bảo mật quan trọng của những "
"tuần cuối cùng. Nếu bạn cần giữ những cố vấn bảo mật phát hành bở Nhóm Bảo "
"mật Debian luôn cập nhật, vui lòng đặt xem dài hạn <a href=\"<get-var url-"
"dsa />\">bó thư bảo mật</a> (và các bó rời <a href=\"<get-var url-bpo />"
"\">bó thư chuyển ngược</a>, và <a href=\"<get-var url-stable-announce />"
"\">bó thư cập nhật ổn định</a> hoặc <a href=\"<get-var url-volatile-"
"announce />\">bó thư hay thay đổi</a>, cho <q><get-var old-stable /></q>, "
"bản phân phối cũ) để biết các thông báo."

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""
"Nhóm phát hành bản ổn định của Debian phát hành một thông báo cập nhật cho "
"gói:"

#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:226
#: ../../english/template/debian/projectnews/boilerplates.wml:357
#: ../../english/template/debian/projectnews/boilerplates.wml:371
msgid ", "
msgstr ", "

#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:227
#: ../../english/template/debian/projectnews/boilerplates.wml:358
#: ../../english/template/debian/projectnews/boilerplates.wml:372
msgid " and "
msgstr " và "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid ". "
msgstr ". "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid "Please read them carefully and take the proper measures."
msgstr "Vui lòng đọc chúng một cách cẩn thận và đo lường thích hợp."

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid "Debian's Backports Team released advisories for these packages: "
msgstr "Nhóm Chuyển ngược Debian phát hành các cố vấn cho những gói: "

#: ../../english/template/debian/projectnews/boilerplates.wml:224
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""
"Nhóm Bảo mật của Debian gần đây phát hành các tư vấn cho những gói sau đây "
"(cùng với những gói khác):"

#: ../../english/template/debian/projectnews/boilerplates.wml:253
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""
"<get-var num-newpkg /> các gói được thêm vào kho Debian dành cho phiên bản "
"chưa ổn định mới gần đây."

#: ../../english/template/debian/projectnews/boilerplates.wml:255
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr " <a href=\"<get-var url-newpkg />\">Giữa nhiều cái khác</a> là:"

#: ../../english/template/debian/projectnews/boilerplates.wml:282
msgid "There are several upcoming Debian-related events:"
msgstr "Có nhiều sự kiện liên-quan-đến-Debian sắp diễn ra:"

#: ../../english/template/debian/projectnews/boilerplates.wml:288
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""
"Bạn có thể tìm thấy nhiều thông tin hơn về các sự kiện và các buổi nói "
"chuyên liên quan đến Debian tại <a href=\"<get-var events-section />\">phần "
"sự kiện</a> của trang thông tin điện tử của Debian, hoặc là đặt xem dài hạn "
"một trong số các bó thư sự kiện cho những khu vực khác nhau: <a href=\"<get-"
"var events-ml-eu />\">Châu Âu</a>, <a href=\"<get-var events-ml-nl />\">Hà "
"Lan</a>, <a href=\"<get-var events-ml-ha />\">Nam Mỹ</a>, <a href=\"<get-var "
"events-ml-na />\">Bắc Mỹ</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:313
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""
"Bạn có muốn tổ chức một gian hàng Debian hay một nhóm cài đặt Debian? Bạn "
"muốn biết các sự kiện có liên quan đến Debian sắp diễn ra? Bạn có buổi nói "
"chuyện mà bạn muốn có liên kết từ <a href=\"<get-var events-talks />\">trang "
"các buổi nói chuyện</a> của chúng tôi? Hãy gửi thư điện tử đến <a href="
"\"<get-var events-team />\">Nhóm sự kiện Debian</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:335
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""
"<get-var dd-num /> đơn đã được <a href=\"<get-var dd-url />\">chấp thuận</a> "
"làm Nhà phát triển Debian"

#: ../../english/template/debian/projectnews/boilerplates.wml:342
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""
"<get-var dm-num /> đơn xin đã được <a href=\"<get-var dm-url />\">chấp "
"thuận</a> là người bảo trì Debian"

#: ../../english/template/debian/projectnews/boilerplates.wml:349
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""
"<get-var uploader-num /> người <a href=\"<get-var uploader-url />\">đã bắt "
"đầu bảo trì các gói</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""
"<get-var eval-newcontributors-text-list /> kể từ lần phát hành trước đây của "
"Tin tức tuần Debian. Chào mừng <get-var eval-newcontributors-name-list /> "
"gia nhập vào dự án của chúng ta!"

#: ../../english/template/debian/projectnews/boilerplates.wml:407
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""
"<get-var issue-devel-news /> phát hành của <a href=\"<get-var url-devel-"
"news />\">tin tức lặt vặt cho các nhà phát triển</a> đã được phát hành và "
"bao gồm các chủ đề sau đây:"

#: ../../english/template/debian/projectnews/footer.wml:6
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Để nhận được thông báo thư này bằng hộp thư điện tử của bạn, <a href="
"\"https://lists.debian.org/debian-news/\">hãy đặt xem dài hạn bó thư debian-"
"news</a>."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr ""
"<a href=\"../../\">Các phát hành trước đây</a> của thư thông báo cũng sẵn có."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Tin tức Dự án Debian được biên tập bởi <a href="
"\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Tin tức Dự án Debian được biên tập bởi <a href="
"\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Phát hành này của Tin tức Dự án Debian được biên tập "
"bởi <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Phát hành này của Tin tức Dự án Debian được biên tập "
"bởi <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr "<void id=\"singular\" />Được dịch bởi %s."

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr "<void id=\"plural\" />Được dịch bởi %s."

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr "<void id=\"singularfemale\" />Được dịch bởi %s."

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr "<void id=\"pluralfemale\" />Được dịch bởi %s."

#: ../../english/template/debian/weeklynews/footer.wml:6
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Để nhận được tin tức này hàng tuần bằng hộp thư điện tử của bạn, <a href="
"\"https://lists.debian.org/debian-news/\">hãy đặt xem dài hạn bó thư debian-"
"news</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Tin tức Tuần Debian được biên tập bởi <a href="
"\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Tin tức Tuần Debian được biên tập bởi <a href=\"mailto:"
"dwn@debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Bản phát hành này của Tin tức Tuần Debian được biên "
"tập bởi <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Bản phát hành này của Tin tức Tuần Debian được biên "
"tập bởi <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "Name:"
#~ msgstr "Tên:"

#~ msgid "Email:"
#~ msgstr "Thư điện tử:"

#~ msgid "Previous Talks:"
#~ msgstr "Buổi nói chuyện trước đây:"

#~ msgid "Languages:"
#~ msgstr "Ngôn ngữ:"

#~ msgid "Location:"
#~ msgstr "Vị trí:"

#~ msgid "Topics:"
#~ msgstr "Chủ đề:"

#~ msgid "List of Speakers"
#~ msgstr "Danh sách người nói chuyện"

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Quay trở lại <a href=\"./\">trang nói chuyện về Debian</a>."

#~| msgid ""
#~| "To receive this newsletter weekly in your mailbox, <a href=\"https://"
#~| "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~| "list</a>."
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Để nhận được tin tức này hai tuần một lần bằng hộp thư điện tử của bạn, "
#~ "<a href=\"https://lists.debian.org/debian-news/\">hãy đặt xem dài hạn bó "
#~ "thư tin-tức-debian</a>."
