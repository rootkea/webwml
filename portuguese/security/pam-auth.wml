#use wml::debian::template title="Detalhes sobre a configuração vulnerável do PAM"
#use wml::debian::translation-check translation="be191e77facf8c0d489cfd320232517e5233a3e2"

<p>Das versões 1.0.1-6 à 1.0.1-9, o utilitário pam-auth-update incluído no
pacote <a href="https://packages.debian.org/libpam-runtime">libpam-runtime</a>
no Debian <em>teste (testing)</em> e <em>instável (unstable)</em> sofria de um bug
pelo qual os sistemas poderiam ser inadvertidamente configurados para permitir
o acesso com ou sem a senha correta
(<a href="https://bugs.debian.org/519927">#519927</a>). Embora a maior parte
dos(as) usuários(as) não devem ter sido afetados(as) por esse bug, aqueles(as)
afetados(as) devem considerar que suas máquinas estão comprometidas,
particularmente se suas máquinas estão configuradas para permitir acesso da
Internet.</p>

<p>Desde a versão 1.0.1-10, lançada em 7 de agosto de 2009, o libpam-runtime
não mais permite esta configuração incorreta e na atualização ele detectará
se seu sistema foi afetado por este bug.</p>

<p>Se uma mensagem foi apresentada a você na atualização, direcionando-o(a)
para esta página web, você deve assumir que seu sistema foi comprometido.
A menos que você esteja familiarizado(a) com a recuperação de falhas de
segurança, vírus e software malicioso, <strong>você deve reinstalar este
sistema a partir do zero</strong> ou conseguir os serviços de um(a)
administrador(a) de sistemas qualificado(a). O
<a href="$(HOME)/doc/manuals/securing-debian-howto/">securing-debian-howto</a>
inclui
<a href="$(HOME)/doc/manuals/securing-debian-howto/ch-after-compromise">informações
sobre recuperação de um sistema comprometido</a>.</p>

<p>O projeto Debian pede desculpas pelo fato de que versões anteriores do
libpam-runtime não detectaram e preveniram esta situação.</p>
