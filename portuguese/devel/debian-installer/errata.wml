#use wml::debian::template title="Errata do Debian-Installer"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5513b0df9f9525c15c9a757a14ac534e8d3ac03e"

<h1>Errata para <humanversion /></h1>

<p>
Esta é uma lista de problemas conhecidos na versão <humanversion /> do
instalador do Debian. Se você não ver seu problema listado aqui, por favor
nos envie (em inglês) um
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">relatório de
instalação</a> descrevendo o problema.
</p>

<dl class="gloss">
     <dt>O GNOME pode falhar ao iniciar com algumas configurações de máquina virtual</dt>
     <dd>Foi observado, durante o teste da imagem Stretch Alpha 4, que o
     GNOME pode falhar ao iniciar dependendo da configuração usada para
     máquinas virtuais. Parece que está normal ao usar o cirrus como um chip de
     vídeo emulado
     <br />
     <b>Estado:</b> Sendo investigado.</dd>

     <dt>Instalações de desktop podem não funcionar usando o CD#1 sozinho</dt>
     <dd>Devido a restrições de espaço no primeiro CD, nem todos os
     pacotes esperados do desktop GNOME cabem no CD#1. Para uma instalação bem
     sucedida, use fontes de pacotes extras (por exemplo, um segundo CD ou um
     espelho de rede), ou use um DVD em vez do CD. <br /> <b>Estado:</b> Não
     é provável que maiores esforços possam ser realizados para encaixar
     mais pacotes no CD#1. </dd>

     <dt>O tema usado no instalador</dt>
     <dd>Ainda não existe a arte do Bullseye, e o instalador ainda está
       usando o tema do Buster.</dd>

     <dt>O LUKS2 é incompatível com o suporte ao cryptodisk do GRUB</dt>
     <dd>Só foi descoberto tardiamente que o GRUB não tem suporte para
       LUKS2. Isto significa que os(as) usuários(as) que queiram usar
       <tt>GRUB_ENABLE_CRYPTODISK</tt> e evitar um <tt>/boot</tt>
       separado e não encriptado, não serão capazes de fazê-lo
       (<a href="https://bugs.debian.org/927165">#927165</a>).  De qualquer
       modo, esta configuração não é suportada pelo instalador, mas faria
       sentido ao menos documentar esta limitação mais proeminentemente,
       e ter ao menos a possibilidade de optar pelo
       LUKS1 durante o processo de instalação.
     <br />
     <b>Estado:</b> Algumas ideias foram expressas sobre este bug;
     os(as) mantenedores(as) do cryptsetup escreveram
     <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">alguma documentação específica</a>.</dd>


<!-- things should be better starting with Jessie Beta 2...
	<dt>GNU/kFreeBSD support</dt>

	<dd>GNU/kFreeBSD installation images suffer from various
	important bugs
	(<a href="https://bugs.debian.org/757985"><a href="https://bugs.debian.org/757985">#757985</a></a>,
	<a href="https://bugs.debian.org/757986"><a href="https://bugs.debian.org/757986">#757986</a></a>,
	<a href="https://bugs.debian.org/757987"><a href="https://bugs.debian.org/757987">#757987</a></a>,
	<a href="https://bugs.debian.org/757988"><a href="https://bugs.debian.org/757988">#757988</a></a>). Porters
	could surely use some helping hands to bring the installer back
	into shape!</dd>
-->

<!-- kind of obsoleted by the first "important change" mentioned in the 20140813 announce...
	<dt>Accessibility of the installed system</dt>
	<dd>Even if accessibility technologies are used during the
	installation process, they might not be automatically enabled
	within the installed system.
	</dd>
-->

<!-- leaving this in for possible future use...
	<dt>Desktop installations on i386 do not work using CD#1 alone</dt>
	<dd>Due to space constraints on the first CD, not all of the expected GNOME desktop
	packages fit on CD#1. For a successful installation, use extra package sources (e.g.
	a second CD or a network mirror) or use a DVD instead.
	<br />
	<b>Status:</b> It is unlikely more efforts can be made to fit more packages on CD#1.
	</dd>
-->

<!-- ditto...
	<dt>Potential issues with UEFI booting on amd64</dt>
	<dd>There have been some reports of issues booting the Debian Installer in UEFI mode
	on amd64 systems. Some systems apparently do not boot reliably using <code>grub-efi</code>, and some
	others show graphics corruption problems when displaying the initial installation splash
	screen.
	<br />
	If you encounter either of these issues, please file a bug report and give as much detail
	as possible, both about the symptoms and your hardware - this should assist the team to fix
	these bugs. As a workaround for now, try switching off UEFI and installing using <q>Legacy
	BIOS</q> or <q>Fallback mode</q> instead.
	<br />
	<b>Status:</b> More bug fixes might appear in the various Wheezy point releases.
	</dd>
-->

<!-- ditto...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>
