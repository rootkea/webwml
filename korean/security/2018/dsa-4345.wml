#use wml::debian::translation-check translation="1d1f8d159bd57a26b5a8603a6dfc4a1937981b1c" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix. The Common Vulnerabilities and
Exposures project identifies the following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

    <p>Florian Stuelpner discovered that Samba is vulnerable to
    infinite query recursion caused by CNAME loops, resulting in
    denial of service.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-14629.html">\
    https://www.samba.org/samba/security/CVE-2018-14629.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16841">CVE-2018-16841</a>

    <p>Alex MacCuish discovered that a user with a valid certificate or
    smart card can crash the Samba AD DC's KDC when configured to accept
    smart-card authentication.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16841.html">\
    https://www.samba.org/samba/security/CVE-2018-16841.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

    <p>Garming Sam of the Samba Team and Catalyst discovered a NULL pointer
    dereference vulnerability in the Samba AD DC LDAP server allowing a
    user able to read more than 256MB of LDAP entries to crash the Samba
    AD DC's LDAP server.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16851.html">\
    https://www.samba.org/samba/security/CVE-2018-16851.html</a></p></li>

</ul>

<p>안정 버전(stretch)에서, 이 문제를 버전 2:4.5.12+dfsg-2+deb9u4에서 수정했습니다.
</p>

<p>samba 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>samba의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4345.data"
