#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794"
<define-tag pagetitle>Debian Edu / Skolelinux Buster — una solución Linux completa para su centro escolar</define-tag>
<define-tag release_date>2019-07-07</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="1.1" maintainer=""

<p>
¿Tiene que administrar un laboratorio de informática o toda la red de un centro escolar?
¿Le gustaría instalar servidores, estaciones de trabajo y ordenadores portátiles que
después trabajarán conjuntamente?
¿Desea la estabilidad de Debian con servicios de red ya
configurados?
¿Le gustaría tener una herramienta web para gestionar sistemas y varios
cientos de cuentas de usuario, o incluso más?
¿Se ha preguntado si las computadoras antiguas pueden ser útiles y cómo?
</p>

<p>
Entonces Debian Edu es para usted. Los propios profesores y profesoras o su personal de soporte técnico
pueden implementar en pocos días un entorno de estudio multiusuario y
multicomputadora. Debian Edu viene con cientos de aplicaciones preinstaladas,
y siempre puede añadir más paquetes de Debian.
</p>

<p>
El equipo de desarrollo de Debian Edu se complace en anunciar Debian Edu 10
<q>Buster</q>, la versión de Debian Edu / Skolelinux basada
en Debian 10 <q>Buster</q>.
Por favor, considere probarlo e informarnos del resultado (&lt;debian-edu@lists.debian.org&gt;)
para ayudarnos a mejorarlo.
</p>

<h2>Acerca de Debian Edu y Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu"> Debian Edu, también conocido como
Skolelinux</a>, es una distribución Linux basada en Debian que proporciona un
entorno de red listo para usar y completamente configurado para un centro escolar.
Inmediatamente tras la instalación se encuentra operativo un servidor ejecutando
todos los servicios necesarios para una red escolar, a la espera de que se añadan
usuarios y máquinas vía GOsa², una cómoda interfaz web.
También queda preparado un entorno de arranque desde red de forma que, tras la
instalación del servidor principal desde CD / DVD / BD o medio extraíble USB, el resto de
máquinas se pueden instalar desde red.
Se pueden utilizar computadoras antiguas (de hasta diez años de antigüedad o así) como clientes
ligeros LTSP o como estaciones de trabajo sin disco, cargando el sistema operativo por red sin
que sean necesarias ninguna instalación ni configuración en absoluto.
El servidor escolar de Debian Edu proporciona una base de datos LDAP y un servicio
de autenticación Kerberos, directorios personales («home») centralizados, un servidor DHCP, un
proxy web y muchos otros servicios. El escritorio contiene más de 60 paquetes
de software educativo, y hay más disponibles en el archivo de Debian.
Los centros escolares pueden elegir entre los entornos de escritorio Xfce, GNOME, LXDE,
MATE, KDE Plasma y LXQt.
</p>

<h2>Novedades en Debian Edu 10 <q>Buster</q></h2>

<p>Estos son algunos puntos de las notas de publicación de Debian Edu 10 <q>Buster</q>,
basado en Debian 10 <q>Buster</q>.
La lista completa, que incluye información más detallada, forma parte del
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">capítulo del manual de Debian Edu</a> correspondiente.
</p>

<ul>
<li>
Ahora hay imágenes de instalación oficiales Debian disponibles.
</li>
<li>
Es posible hacer una instalación modular específica para el sitio.
</li>
<li>
Se proporcionan metapaquetes adicionales que agrupan paquetes educativos por nivel
escolar.
</li>
<li>
Configuración regional del escritorio mejorada para todos los idiomas soportados por Debian.
</li>
<li>
Herramienta disponible para facilitar la configuración del soporte multiidioma específico para el sitio.
</li>
<li>
Se ha añadido gestión de contraseñas de GOsa²-Plugin («GOsa²-Plugin Password Management»).
</li>
<li>
Mejorado el soporte de TLS/SSL en la red interna.
</li>
<li>
La configuración de Kerberos soporta los servicios NFS y SSH.
</li>
<li>
Hay una herramienta disponible para regenerar la base de datos LDAP.
</li>
<li>
El servidor X2Go se instala en todos los sistemas con perfil LTSP-Server.
</li>
</ul>

<h2>Opciones de descarga, pasos de instalación y manual</h2>

<p>
Hay disponibles imágenes de CD del instalador de red para PC
de 64 y de 32 bits. En raras ocasiones (para PC de más de unos doce años) se
necesitará la imagen de 32 bits. Las imágenes se pueden descargar de las ubicaciones
siguientes:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternativamente, también hay disponibles imágenes extendidas de BD (de
más de 5 GB). Es posible configurar una red Debian Edu completa sin
conexión a Internet (todos los entornos de escritorio soportados, todos los idiomas soportados
por Debian). Estas imágenes se pueden descargar de las ubicaciones siguientes:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Las imágenes se pueden verificar utilizando las sumas de verificación («checksums») firmadas disponibles en el directorio
de descarga.
<br />
Una vez descargada una imagen, puede comprobar que:
</p>

<ul>
<li>
su suma de verificación coincide con la indicada en el fichero de verificación; y que
</li>
<li>
el fichero de verificación no ha sido alterado.
</li>
</ul>

<p>
Para más información sobre cómo hacerlo, lea la
<a href="https://www.debian.org/CD/verify">guía de verificación</a>.
</p>

<p>
Debian Edu 10 <q>Buster</q> está basado completamento en Debian 10 <q>Buster</q>; por lo tanto, en el
archivo Debian están disponibles los fuentes de todos los paquetes.
</p>

<p>
Tenga en cuenta la
<a href="https://wiki.debian.org/DebianEdu/Status/Buster">página de estado de Debian Edu Buster</a>
para información actualizada sobre Debian Edu 10 <q>Buster</q>, incluyendo
instrucciones para el uso de <code>rsync</code> para descargar las imágenes ISO.
</p>

<p>
Para actualizaciones desde Debian Edu 9 <q>Stretch</q> lea el
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">capítulo del manual de Debian Edu</a> correspondiente.
</p>

<p>
Para notas de instalación lea el
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">capítulo del manual de Debian Edu</a> correspondiente.
</p>

<p>
Después de la instalación tiene que realizar estos
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">primeros pasos</a>.
</p>

<p>
Consulte la <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">wiki de Debian Edu</a>
para acceder a la versión más reciente en inglés del manual de Debian Edu <q>Buster</q>.
El manual ha sido traducido en su integridad al alemán, francés, italiano, danés, holandés,
noruego bokmål y japonés. También hay versiones traducidas parcialmente al español
y al chino simplificado.
Tiene disponibles <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
las últimas versiones traducidas del manual</a>.
</p>

<p>
En las notas de publicación y en el manual de instalación hay más información sobre
el propio Debian 10 <q>Buster</q>; vea <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;press@debian.org&gt;.</p>
