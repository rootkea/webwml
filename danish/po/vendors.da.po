msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:53+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Forhandler"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Tillader bidrag"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arkitekturer"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Sender til udlandet"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontakt"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Forhandlerside"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "side"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "e-mail"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "indenfor Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "til visse områder"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "kildekode"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "og"

#~ msgid "updated monthly"
#~ msgstr "opdateres månedligt"

#~ msgid "updated twice weekly"
#~ msgstr "opdateres to gange om ugen"

#~ msgid "updated weekly"
#~ msgstr "opdateres ugentligt"

#~ msgid "reseller"
#~ msgstr "forhandler"

#~ msgid "reseller of $var"
#~ msgstr "forhandler af $var"

#~ msgid "Custom Release"
#~ msgstr "Specialfremstillet udgave"

#~ msgid "vendor additions"
#~ msgstr "tillæg fra distributør"

#~ msgid "contrib included"
#~ msgstr "inkluderer contrib-delen"

#~ msgid "non-free included"
#~ msgstr "inkluderer non-free-delen"

#~ msgid "non-US included"
#~ msgstr "inkluderer non-US-delen"

#~ msgid "Multiple Distribution"
#~ msgstr "Flere distributioner"

#~ msgid "Vendor Release"
#~ msgstr "Distributørspecifik udgave"

#~ msgid "Development Snapshot"
#~ msgstr "Udviklingsversion"

#~ msgid "Official DVD"
#~ msgstr "Officiel dvd"

#~ msgid "Official CD"
#~ msgstr "Officiel cd"

#~ msgid "Architectures:"
#~ msgstr "Arkitekturer:"

#~ msgid "USB Type:"
#~ msgstr "USB-type:"

#~ msgid "BD Type:"
#~ msgstr "BD-type:"

#~ msgid "DVD Type:"
#~ msgstr "DVD-type"

#~ msgid "CD Type:"
#~ msgstr "CD-type:"

#~ msgid "email:"
#~ msgstr "e-mail:"

#~ msgid "Ship International:"
#~ msgstr "Sender til udlandet:"

#~ msgid "Country:"
#~ msgstr "Land:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Tillader bidrag til Debian:"

#~ msgid "URL for Debian Page:"
#~ msgstr "Debian-sidens URL:"

#~ msgid "Vendor:"
#~ msgstr "Forhandler:"
